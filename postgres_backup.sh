#!/bin/bash


if [ "$#" != 3 ]
then
        echo "Usage : postgres_backup_rbc.sh {pg_host} {pg_user} {pg_pass(optional)}"
        exit 1
fi

pg_host=$1
pg_user=$2
pg_pass=$3


ts=$(date '+%Y%m%d.%H%M%S')

backup_path=$PWD/backup/postgres_$HOSTNAME
mkdir -p $backup_path
mkdir -p $backup_path/log
touch $backup_path/log/backup_postgres.log.$
db_list="ambari ranger yahoo"


{
echo "Creating pg  Backup :$ts"
for db in $db_list
do
  pg_dump -h $pg_host -U $pg_user $db > $backup_path/${db}_dump_$ts.ddl 
  if [ $? -eq 0 ]; then
    echo "Creating pg DB $db Backup Successful : $ts"
  else
    echo "Creating pg DB $db Backup Failed : $ts"
    exit 1;                
  fi;
done

echo "Creating tar archive for db backup"
cd $backup_path
tar -cvzf $backup_path/pg_backup_$ts.tar *$ts.ddl
if [ $? -eq 0 ]; then
  echo "Archive done successfully"
  rm -f $backup_path?/*.ddl
  find $backup_path/*.tar -mtime +30 -exec rm -f {} \;
else
  echo "Archive failed"
  exit 1;
fi;
} > $backup_path/log/backup_pg.log.$ts 2>&1




#pg_dump -h $HOSTNAME -U ambari  ambari  > all_pg_db
