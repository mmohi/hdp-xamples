#!/bin/bash



if [ "$#" != 4 ]
then
        echo "Usage : cluster_conf_fs_image_backup.sh {AMBARI_HOST} {AMBARI_USER} {AMBARI_PASSWORD} {CLUSTER_NAME}"
        exit 1
fi


AMBARI_HOST=$1
AMBARI_USER=$2
AMBARI_PASSWORD=$3
CLUSTER_NAME=$4
AMBARI_PORT=8080

ts=$(date '+%Y%m%d.%H%M%S')

backup_path=$PWD/backup/cluster_conf_backup/$(hostname -f)_$ts
from_ambari=$backup_path/from_ambari
conf_backup=$backup_path/conf_backup
fs_image=$backup_path/fs_image

mkdir -p $backup_path $from_ambari $conf_backup $fs_image


# Backing up Service configurations from Ambari 

for CONFIG_TYPE in `curl -s -u $AMBARI_USER:$AMBARI_PASSWORD http://$AMBARI_HOST:$AMBARI_PORT/api/v1/clusters/$CLUSTER_NAME/?fields=Clusters/desired_configs | grep '" : {' | grep -v Clusters | grep -v desired_configs | cut -d'"' -f2`; do
    echo "backuping $CONFIG_TYPE"
    /var/lib/ambari-server/resources/scripts/configs.sh -u $AMBARI_USER -p $AMBARI_PASSWORD -port $AMBARI_PORT get $AMBARI_HOST $CLUSTER_NAME $CONFIG_TYPE | grep '^"' | grep -v '^"properties" : {' > $backup_path/from_ambari/$CONFIG_TYPE.conf
done

tar -cvzf $conf_backup/all_conf_$ts.tar $backup_path/from_ambari/*.conf

# Backing up Service configurations from /usr/hdp/current directory [ backup plan for backup failures]
echo "Backing up Service configurations from /usr/hdp/current directory [ backup plan for backup failures]"
tar -cvzf $conf_backup/hdp_current_$ts.tar /usr/hdp/current


# Backing up Namenode and Secondary Namenode fs image. This script assumes the name node is running in the same node as Ambari. 
echo "Backing up Namenode fs image" 
tar -cvzf $fs_image/nn_fs_image.tar /hadoop/hdfs/namenode


# SNN FS image back is not required. But if needed, should be ran in the SNN node. 
#echo "Backing up Secondary Namenode fs image" 
# tar -cvzf $fs_image/snn_fs_image.tar /hadoop/hdfs/namesecondary

