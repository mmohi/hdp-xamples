#!/usr/bin/python
import socket, os, sys, ConfigParser,logging


from optparse import OptionParser
from cm_api.api_client import ApiResource
from cm_api.endpoints.types import *
from cm_api.endpoints.cms import ClouderaManager
from cm_api.endpoints.services import *
from time import sleep

HOST_CLUSTER=[]
ADD_HOST=[]
HOST_REQ=[]
CLUSTER_HOSTS=[]
CMD_WAIT=15
CMD_TIMEOUT=60


# Create logging 
def logger(logType,msg):
	'''
Logger v1
logType : [debug|info|warn|error|critical]
msg : Log message
:return:
	'''
	logging.basicConfig(filename=LOG_FILE,level=logging.DEBUG,format='%(asctime)s.%(msecs)d %(levelname)s %(module)s - %(funcName)s: %(message)s', datefmt="%Y-%m-%d %H:%M:%S")
	
	if logType == 'debug': logging.debug(msg)
	elif logType == 'info': logging.info(msg) 
	elif logType == 'warn': logging.warning(msg) 
	elif logType == 'error': logging.error(msg) 
	elif logType == 'critical': logging.critical(msg) 
	return True;


# Creates the cluster and adds hosts
def init_cluster(api, cluster_name, cdh_version,hosts,cm_host):
	try :
		cluster = api.create_cluster(cluster_name, cdh_version)
		logger('info', "Creating cluster:" + cluster_name)
		all_hosts = list(hosts)
		# all_hosts.append(cm_host)
		cluster.add_hosts(all_hosts)
		deploy_parcels(CLUSTER, PARCELS)
		logger('info', "Downloaded and distributed parcels: ")
	except:
		logger('warn',"Cluster already created with same name"+cluster_name);
		cluster = api.get_cluster(cluster_name)
	return cluster

def install_hosts(api,host_username,host_list,host_pkey,cm_repo_url):
	'''
	install_hosts: V1 
	Install Cloudera Manager Agent on a set of hosts.
	:return:
	'''
	cmd = api.host_install(host_username, host_list, private_key=host_pkey, passphrase=None, cm_repo_url=cm_repo_url,unlimited_jce=True).wait()
	logger('info',"Installing hosts. This might take a while.");
	while cmd.success == None:
		cmd = cmd.fetch()
		logger('Info',"cm_host_install Success: " + str(cmd.resultMessage))
		if cmd.success != True:
			logger('error',"cm_host_install failed: " + str(cmd.resultMessage))
			return False;
		else: return True

def install_hosts_pass(api,host_username,host_list,host_pass,cm_repo_url):
        '''
        install_hosts: V1
        Install Cloudera Manager Agent on a set of hosts.
        :return:
        '''
        cmd = api.host_install(host_username, host_list,password=host_pass,cm_repo_url=cm_repo_url,unlimited_jce=True).wait()
        logger('info',"Installing hosts. This might take a while.");
        while cmd.success == None:
                sleep(5)
                cmd = cmd.fetch()
                logger('Info',"cm_host_install Success: " + str(cmd.resultMessage))
                if cmd.success != True:
                        logger('error',"cm_host_install failed: " + str(cmd.resultMessage))
                        exit(0)
        	else: return True


def add_host (logger,api,svr_list):

   #for server_add_request in svr_list.split(','):
   for server_add_request in svr_list:
       HOST_REQ.append(socket.gethostbyname(server_add_request))
   for host in api.get_all_hosts():
       HOST_CLUSTER.append(socket.gethostbyname(host.hostname))
   logger('info',"List of hosts to be added to Cloudera Manager :" +  str(set(HOST_REQ)-set(HOST_CLUSTER)))
   for name in list(set(HOST_REQ)-set(HOST_CLUSTER)):
     try:
       host = api.create_host(
         name,                             # Host id
         socket.getfqdn(name),       		# Host name (FQDN)
         socket.gethostbyname(name),       # IP address
         "/default_rack")                  # Rack
       hosts.append(host)
       logger('info',"Host " + socket.getfqdn(name) + " added successfully to Cloudera Manager")
     except:
       logger('warn',"Host " +  socket.getfqdn(name) + " already part of Cloudera Manager")

def deploy_management(logger,cm,cm_host=None,pass_suffix=None):

       ### Management Services ###
	MGMT_SERVICENAME = "CLOUDERA_MGMT"

	MGMT_SERVICE_CONFIG = {
		'zookeeper_datadir_autocreate': 'true',
	}
	MGMT_ROLE_CONFIG = {
		'quorumPort': 2888,
	}
	
	APUB_ROLENAME = "ALERTPUBLISHER"
	APUB_ROLE_CONFIG = {
			
	}
	ESERV_ROLENAME = "EVENTSERVER"
	ESERV_ROLE_CONFIG = {
		'event_server_heapsize': '215964392'  }
	
	HMON_ROLENAME = "HOSTMONITOR"
	HMON_ROLE_CONFIG = {}
	SMON_ROLENAME = "SERVICEMONITOR"
	SMON_ROLE_CONFIG = {
                'firehose_database_host': cm_host + ":3306",
                'firehose_database_user': "smon",
                'firehose_database_password': "smon_" + pass_suffix ,
                'firehose_database_type': 'mysql',
                'firehose_database_name': "smon",
                'firehose_heapsize': '1073741824' }

	NAV_ROLENAME = "NAVIGATOR"
	NAV_ROLE_CONFIG = {
		'navigator_database_host': cm_host + ":3306",
		'navigator_database_user': "nav",
		'navigator_database_password': "nav_" + pass_suffix ,
		'navigator_database_type': 'mysql',
		'navigator_database_name': "nav",
		'navigator_heapsize': '1073741824' }
	
	NAVMS_ROLENAME = "NAVIGATORMETASERVER"
	NAVMS_ROLE_CONFIG = {
                'nav_metaserver_database_host': cm_host + ":3306",
                'nav_metaserver_database_user': 'navms',
                'nav_metaserver_database_password': 'navms_' + pass_suffix ,
                'nav_metaserver_database_type': 'mysql',
                'nav_metaserver_database_name': 'navms' }

	RMAN_ROLENAME = "REPORTMANAGER"
	RMAN_ROLE_CONFIG = {
		'headlamp_database_host': cm_host + ":3306",
		'headlamp_database_user': 'rman',
		'headlamp_database_name': 'rman',
		'headlamp_database_password': 'rman_' + pass_suffix ,
		'headlamp_database_type': 'mysql',
		'headlamp_heapsize': '1073741824' }
           
	mgmt=None
	try:
		logger('info',"Creating Management service : "+str(MGMT_SERVICENAME) + str(cm_host))
		service_setup = ApiServiceSetupInfo(name=MGMT_SERVICENAME, type="MGMT")
		mgmt = cm.create_mgmt_service(service_setup)

		logger('debug',"Creating Management Service Instance: "+str(MGMT_SERVICENAME))
		logger('debug',"Waiting for CMS "+str(MGMT_SERVICENAME))
		sleep(CMD_WAIT)

                logger('info',"Creating role for SERVICEMONITOR")
		cmd = mgmt.create_role(SMON_ROLENAME , "SERVICEMONITOR",cm_host)
		sleep(CMD_WAIT)

                logger('info',"Creating role for HOSTMONITOR")
		cmd = mgmt.create_role(HMON_ROLENAME, "HOSTMONITOR", cm_host)
		sleep(CMD_WAIT)

                logger('info',"Creating role for EVENTSERVER")
		cmd = mgmt.create_role(ESERV_ROLENAME, "EVENTSERVER", cm_host)
		sleep(CMD_WAIT)

                logger('info',"Creating role for ALERTPUBLISHER")
		cmd = mgmt.create_role(APUB_ROLENAME, "ALERTPUBLISHER", cm_host)
		sleep(CMD_WAIT)

                logger('info',"Creating role for REPORTMANAGER")
		cmd = mgmt.create_role(RMAN_ROLENAME, "REPORTSMANAGER", cm_host)
		sleep(CMD_WAIT)

                logger('info',"Creating role for NAVIGATOR")
		cmd = mgmt.create_role(NAV_ROLENAME, "NAVIGATOR", cm_host)
		sleep(CMD_WAIT)

                logger('info',"Creating role for NAVIGATORMETASERVER")
		cmd = mgmt.create_role(NAVMS_ROLENAME,"NAVIGATORMETASERVER", cm_host)
		sleep(CMD_WAIT)
		
		# now configure each role   
		for group in mgmt.get_all_role_config_groups():
			if group.roleType == "ALERTPUBLISHER":
                                logger('info',"Update Config for " + str(group.roleType))
				group.update_config(APUB_ROLE_CONFIG)
			elif group.roleType == "EVENTSERVER":
                                logger('info',"Update Config for " + str(group.roleType))
				group.update_config(ESERV_ROLE_CONFIG)
			elif group.roleType == "HOSTMONITOR":
                                logger('info',"Update Config for " + str(group.roleType))
				group.update_config(HMON_ROLE_CONFIG)
			elif group.roleType == "SERVICEMONITOR":
                                logger('info',"Update Config for " + str(group.roleType))
				group.update_config(SMON_ROLE_CONFIG)
			elif group.roleType == "NAVIGATOR":
                                logger('info',"Update Config for " + str(group.roleType))
				group.update_config(NAV_ROLE_CONFIG)
			elif group.roleType == "NAVIGATORMETASERVER":
                                logger('info',"Update Config for " + str(group.roleType))
				group.update_config(NAVMS_ROLE_CONFIG)
			elif group.roleType == "REPORTSMANAGER":
                                logger('info',"Update Config for " + str(group.roleType))
				group.update_config(RMAN_ROLE_CONFIG)
		return mgmt				
	except Exception,e:
		logger('debug',"Error Code : " + str(e))
		logger('warn',"Management service already created "+str(MGMT_SERVICENAME))
		print "Management service already created "+str(MGMT_SERVICENAME)
		return False		
			
def start_management(logger,self):
	cm_service = self.get_service()
	cmd = str(cm_service.serviceState)
	if cmd != "STARTED":
		try :
			cmd = cm_service.start().wait()
			while cmd.success == None:
				cmd = cmd.fetch()
				logger('Info',"Cloudera Management service started successfully: " + str(cmd.resultMessage))
				sleep(5)
			if cmd.success != True:
				logger('error',"Cloudera Management service failed to start: " + str(cmd.resultMessage))
				exit(0)
		except:
			logger('critical',"Cloudera Management service failed to intialize: ")
	else:
		logger('info',"Cloudera Management service already started")
		print "Cloudera Management service already started\n"


# Downloads and distributes parcels
def deploy_parcels(cluster, parcels):
   for parcel in parcels:
      p = cluster.get_parcel(parcel['name'], parcel['version'])
      p.start_download()
      while True:
         p = cluster.get_parcel(parcel['name'], parcel['version'])
         if p.stage == "DOWNLOADED":
            break
         if p.state.errors:
            raise Exception(str(p.state.errors))
         print "Downloading %s: %s / %s" % (parcel['name'], p.state.progress, p.state.totalProgress)
         time.sleep(15)
      print "Downloaded %s" % (parcel['name'])
      p.start_distribution()
      while True:
         p = cluster.get_parcel(parcel['name'], parcel['version'])
         if p.stage == "DISTRIBUTED":
            break
         if p.state.errors:
            raise Exception(str(p.state.errors))
         print "Distributing %s: %s / %s" % (parcel['name'], p.state.progress, p.state.totalProgress)
         time.sleep(15)
      print "Distributed %s" % (parcel['name'])
      p.activate()

def hosts_inspect(self):
	cmd = self.inspect_hosts()
	while cmd.success == None:
		cmd = cmd.fetch()
		sleep(5)
	if cmd.success != True:
		logger('critical',"Host inpsection failed!")
		exit(0)
	
	logger('info',"Hosts successfully inspected: \n" + str(cmd.resultMessage))

def start_cluster(logger,cluster_name):
#   This will start all services of a cluster
        try :
                cluster = api.create_cluster(cluster_name, cdh_version)
                logger('info', "Starting cluster:" + cluster_name)
                cluster.start().wait()
                logger('info', "Cluster started successfully")
        except:
                logger('warn',"Cluster start failed")
                return False;




def oozie_setup(logger,service):
	'''
Oozie Setup creates the command creates only tables required by Oozie
Upgrade Oozie Database schema as part of a major version upgrade
Module : deploy_CMS
 :return:
	'''
	#Create Database Schema in configured database
	#Upgrade Database Schema
	cmd = service.stop().wait()
	cmd = service.create_oozie_db().wait()
	cmd = service.upgrade_oozie_db().wait()
	cmd = service.install_oozie_sharelib().wait()
	cmd =  service.restart().wait()
	logger('info','Oozie Upgrade Done Successfully');

def hive_setup(logger,service):
        '''
Hive Setup creates the command creates metastore tables required by Hive
Create Hive userdir and  warehouse  
Module : deploy_CMS
 :return:
        '''
        #Create Database Schema in configured database
        #Upgrade Database Schema
        # cmd = service.create_hive_metastore_tables().wait()
        cmd = service.create_hive_userdir().wait()
        cmd = service.create_hive_warehouse().wait()
        cmd =  service.restart().wait()
        logger('info','Hive Upgrade Done Successfully');

	
def service_assign(self):
	basic_service_group = ['HIVE','YARN','OOZIE','SQOOP_CLIENT','HDFS','ZOOKEEPER','HUE']
	medium_service_group = ['HIVE','YARN','OOZIE','SQOOP_CLIENT','HDFS','ZOOKEEPER','IMPALA','SPARK_ON_YARN','HUE']
	full_service_group = ['SPARK_ON_YARN','HIVE', 'HBASE', 'OOZIE', 'SQOOP_CLIENT', 'HDFS', 'SPARK', 'YARN', 'HUE', 'ZOOKEEPER', 'IMPALA']
	
	if ( SERVICE_GROUP == 'basic'): service_types = basic_service_group;
	elif (SERVICE_GROUP == 'medium'): service_types = medium_service_group;
	elif (SERVICE_GROUP == 'full'): service_types = full_service_group;
	
	try:
		for service in service_types:
			self.create_service(name=service.lower(), service_type=service.upper())
			logger('info',"Assigning Service on cluster: "+CLUSTER_NM + "services: " + str(service_types))
	except:
		logger('critical',"Unable to assign services on cluster: "+ CLUSTER_NM)
		exit(0)
	return True

# if __name__ == "__main__":
    # main()
