######### MySQL database restore process ###########
 
1. To restore from a backup, we will delete the database 'google'

mysql> show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| google             |
| hive               |
| mysql              |
| performance_schema |
| ranger             |
+--------------------+
6 rows in set (0.00 sec)

mysql> use google;
Database changed
mysql> show tables;
+------------------+
| Tables_in_google |
+------------------+
| another_tbl      |
+------------------+
1 row in set (0.00 sec)

mysql> select * from another_tbl;
+-------------+----------------+-----------------+-----------------+
| tutorial_id | tutorial_title | tutorial_author | submission_date |
+-------------+----------------+-----------------+-----------------+
|           1 | Learn PHP      | John Poul       | 2017-11-19      |
|           2 | Learn MySQL    | Abdul S         | 2017-11-19      |
|           3 | JAVA Tutorial  | Sanjay          | 2007-05-06      |
+-------------+----------------+-----------------+-----------------+
3 rows in set (0.00 sec)

mysql> drop database google;
Query OK, 1 row affected (0.01 sec)

mysql> show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| hive               |
| mysql              |
| performance_schema |
| ranger             |
+--------------------+
5 rows in set (0.00 sec)


2. To restore any particular database from a back up, create the database first 

mysql> create database google;
Query OK, 1 row affected (0.00 sec)

mysql> exit

3. Run the following command specifying the destination database and the source backup dump

[root@sandbox admin]# mysql -uroot -p google < backup/mysql_sandbox.hortonworks.com/google_dump_20171119.032911.ddl


4. Verify the restored database and tables

[root@sandbox admin]# mysql -uroot -p

mysql> use google;

Database changed
mysql> show tables;
+------------------+
| Tables_in_google |
+------------------+
| another_tbl      |
+------------------+
1 row in set (0.00 sec)

mysql> select * from another_tbl;
+-------------+----------------+-----------------+-----------------+
| tutorial_id | tutorial_title | tutorial_author | submission_date |
+-------------+----------------+-----------------+-----------------+
|           1 | Learn PHP      | John Poul       | 2017-11-19      |
|           2 | Learn MySQL    | Abdul S         | 2017-11-19      |
|           3 | JAVA Tutorial  | Sanjay          | 2007-05-06      |
+-------------+----------------+-----------------+-----------------+
3 rows in set (0.00 sec)



######### PostgreSQL database restore process ###########

1. To restore from a backup, we will delete the database 'yahoo'


ambari=> \c yahoo
psql (8.4.20)
You are now connected to database "yahoo".
yahoo=> select * from myschema.company;
 id | name  | age |                      address                       | salary | join_date
----+-------+-----+----------------------------------------------------+--------+------------
  1 | Paul  |  32 | California                                         |  20000 | 2001-07-13
  2 | Allen |  25 | Texas                                              |        | 2007-12-13
  3 | Teddy |  23 | Norway                                             |  20000 |
  4 | Mark  |  25 | Rich-Mond                                          |  65000 | 2007-12-13
  5 | David |  27 | Texas                                              |  85000 | 2007-12-13
(5 rows)

ambari=> drop database yahoo;
DROP DATABASE
ambari=> \l
                                  List of databases
   Name    |  Owner   | Encoding |  Collation  |    Ctype    |   Access privileges
-----------+----------+----------+-------------+-------------+-----------------------
 ambari    | postgres | UTF8     | en_US.UTF-8 | en_US.UTF-8 | =Tc/postgres
                                                             : postgres=CTc/postgres
                                                             : ambari=CTc/postgres
 postgres  | postgres | UTF8     | en_US.UTF-8 | en_US.UTF-8 |
 template0 | postgres | UTF8     | en_US.UTF-8 | en_US.UTF-8 | =c/postgres
                                                             : postgres=CTc/postgres
 template1 | postgres | UTF8     | en_US.UTF-8 | en_US.UTF-8 | =c/postgres
                                                             : postgres=CTc/postgres
(4 rows)


2. To restore any particular database from a back up, create the database first, as postgres user. Change the owner of the database to the 'ambari' user. 

postgres=# create database yahoo;
CREATE DATABASE
postgres=# ALTER DATABASE yahoo OWNER TO ambari;
ALTER DATABASE


3. Run the dump restore command as 'ambari' user 


psql -h sandbox -U ambari yahoo < yahoo.ddl
Password for user ambari:


4. Verify the restore 

ambari=> \c yahoo
psql (8.4.20)
You are now connected to database "yahoo".

yahoo=> select * from myschema.company;
 id | name  | age |                      address                       | salary | join_date
----+-------+-----+----------------------------------------------------+--------+------------
  1 | Paul  |  32 | California                                         |  20000 | 2001-07-13
  2 | Allen |  25 | Texas                                              |        | 2007-12-13
  3 | Teddy |  23 | Norway                                             |  20000 |
  4 | Mark  |  25 | Rich-Mond                                          |  65000 | 2007-12-13
  5 | David |  27 | Texas                                              |  85000 | 2007-12-13
(5 rows)












