echo "Enter Ambari Admin id"
read AMBARI_USER


unset password
echo "Enter Ambari Admin Password"
while IFS= read -p "$prompt" -r -s -n 1 char
do
    if [[ $char == $'\0' ]]
    then
        break
    fi
    prompt='*'
    password+="$char"

done
echo

echo "Enter Ambari Hostname"
read AMBARI_HOST

echo "Enter Cluster Name"
read CLUSTER_NAME


#export AMBARI_USER=raj_ops
#export AMBARI_PASSWD=raj_ops
#export AMBARI_HOST=127.0.0.1
#export CLUSTER_NAME=Sandbox



for i in `curl -iku $AMBARI_USER:$AMBARI_PASSWD -H "X-Requested-By: ambari" -s $AMBARI_HOST:8080/api/v1/clusters/$CLUSTER_NAME/?fields=Clusters/desired_configs | grep '" : {' | grep -v Clusters | grep -v desired_configs | cut -d'"' -f2 | grep -v "ranger-site\|log4\|slider\|usersync-properties"`
do

output="$(/var/lib/ambari-server/resources/scripts/configs.py -port 8080  -a get -l $AMBARI_HOST -n $CLUSTER_NAME  -u $AMBARI_USER -p $AMBARI_PASSWD  -c $i | grep "log.location\|logdir\|log-dir\|log_dir" | grep -v "suffix\|content\|export\|path\|OPTS"| sed "s/^[ \t]*//")"
if [[ $output ]]
then
echo -e "\033[1m $i \033[0m" ::: $output "\n"
fi


done

unset password