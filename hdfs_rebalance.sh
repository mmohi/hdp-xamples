#!/bin/bash
 
if [ "$#" != 2 ]
then
        echo "Usage : hdfs_balancer.sh {keytab} {principal}"
        exit 1
fi
 
#Obtain TGT for kerberos
kinit -kt $1 $2
 
ts=$(date '+%Y%m%d-%H:%M:%S')
 
echo "*********************************"
echo "Starting the hdfs balancer : $ts"
 
sudo -u hdfs hdfs balancer
 
if [ $? -eq 0 ]
then
    echo "hdfs balancer completed succesfully : $ts"
else
    echo "hdfs balancer failed : $ts"
    exit 1
fi
done
echo "*********************************"
