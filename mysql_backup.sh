#!/bin/bash


if [ "$#" != 3 ]
then
        echo "Usage : mysql_backup_rbc.sh {mysql_host} {mysql_user} {mysql_password(optional)}"
        exit 1
fi

mysql_host=$1
mysql_user=$2
mysql_pass=$3


ts=$(date '+%Y%m%d.%H%M%S')

backup_path=$PWD/backup/mysql_$HOSTNAME
mkdir -p $backup_path
mkdir -p $backup_path/log
touch $backup_path/log/backup_mysql.log.$ts



{
echo "Creating mysql  Backup :$ts"
for db in $(mysql -h$mysql_host -u$mysql_user -p$mysql_pass --skip-column-names -e  "show databases"|grep -vwE "nav|information_schema|performance_schema" )
do
  mysqldump -h$mysql_host -u$mysql_user -p$mysql_pass  --add-drop-database --add-drop-table --allow-keywords --flush-privileges --result-file $backup_path/${db}_dump_$ts.ddl --databases $db
  if [ $? -eq 0 ]; then
    echo "Creating mysql DB $db Backup Successful : $ts"
  else
    echo "Creating mysql DB $db Backup Failed : $ts"
    exit 1;
  fi;
done

echo "Creating tar archive for db backup"
cd $backup_path
tar -cvzf $backup_path/backup_$ts.tar *$ts.ddl
if [ $? -eq 0 ]; then
  echo "Archive done successfully"
  rm -f $backup_path?/*.ddl
  find $backup_path/*.tar -mtime +30 -exec rm -f {} \;
else
  echo "Archive failed"
  exit 1;
fi;
} > $backup_path/log/backup_mysql.log.$ts 2>&1
